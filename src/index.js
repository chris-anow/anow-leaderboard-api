// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
import express from 'express';
import expressWs from 'express-ws';
import debug from 'debug';
import cors from 'cors';

const log = debug('anow:leaderboard');
const error = debug('anow:error');
const port = process.env.PORT || 3000; // set our port
const minRefresh = process.env.MIN_REFRESH || 10; // minimum amount of time to wait between updates
const maxRefresh = process.env.MAX_REFRESH || 60; // minimum amount of time to wait between updates

function random(low, high) {
  return Math.floor((Math.random() * ((high - low) + 1)) + low);
}

let sockets = [];

const leaderboard = [{
  name: 'Dan',
  wins: 0,
  losses: 0,
  elo: 0,
}, {
  name: 'Stephen',
  wins: 0,
  losses: 0,
  elo: 0,
}, {
  name: 'Sarah',
  wins: 0,
  losses: 0,
  elo: 0,
}, {
  name: 'Chris',
  wins: 0,
  losses: 0,
  elo: 0,
}, {
  name: 'Trista',
  wins: 0,
  losses: 0,
  elo: 0,
}, {
  name: 'Marty',
  wins: 0,
  losses: 0,
  elo: 0,
}, {
  name: 'Greg',
  wins: 0,
  losses: 0,
  elo: 0,
}];

function updateLeaderboard() {
  log('Updating leaderboard..');
  leaderboard.forEach((person) => {
    // for some reason eslint thinks we are reassigning to person, not just updating properties
    // so we just diable this for now.
    /* eslint-disable no-param-reassign */
    person.wins += random(0, 2);
    person.losses += random(0, 2);
    person.elo += random(-5, 5);
    /* eslint-enable no-param-reassign */
  });

  if (sockets.length > 0) {
    log(`Notifying ${sockets.length} web sockets of update...`);
    sockets.forEach(socket => socket.send(JSON.stringify(leaderboard)));
  }

  const timeout = 1000 * random(minRefresh, maxRefresh);
  log(`Next update in ~${timeout / 1000}s.`);
  setTimeout(updateLeaderboard, timeout);
}

const app = express(); // define our app using express
app.use(cors());
expressWs(app);

// ROUTES FOR OUR API
// =============================================================================
const router = express.Router(); // eslint-disable-line new-cap

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/leaderboard', (req, res) => {
  res.json(leaderboard);
});

router.ws('/leaderboard', (socket) => {
  sockets = sockets.concat(socket);
  log(`WebSocket connected (${sockets.length} current)`);

  socket.on('close', () => {
    sockets = sockets.filter(s => s !== socket);
    log(`WebSocket disconnected (${sockets.length} remain)`);
  });

  socket.on('error', (err) => {
    error('A web socket error occured:');
    error(err.stack);
  });
});
// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/', router);

// START THE SERVER
// =============================================================================
app.listen(port, (err) => {
  if (err) {
    error(`Unable to start server: ${err}`);
    process.abort();
  }
  log('Listening on:');
  log(`  http://localhost:${port}/leaderboard`);
  log(`  ws://localhost:${port}/leaderboard`);
});

// START the background task that updates the leaderboard
updateLeaderboard();
